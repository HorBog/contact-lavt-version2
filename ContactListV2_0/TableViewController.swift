//
//  TableViewController.swift
//  ContactListV2_0
//
//  Created by Богдан on 18.05.17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    private let nc = NotificationCenter.default
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    @IBOutlet weak var btnSortContacts: UIBarButtonItem!
    private var typeOfSort = "FirstName"
    private var sortedContacsList: [User] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactList.downloadContacts()
        btnEditOnOff()
        btnSortContactOnOff()
        setSortedContactList()
        
        nc.addObserver(forName: NSNotification.Name(rawValue:"ContactWasFullyChanged"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: NSNotification.Name(rawValue:"ContactWasChanged"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: NSNotification.Name(rawValue:"ContactDeleted"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: NSNotification.Name(rawValue:"EditedContactWasDeleted"), object: nil, queue: nil, using: catchNotification)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    func catchNotification(notification:Notification) -> Void {
        
        if notification.name.rawValue == "ContactWasChanged" {
            //find method how to rewrite only one cell without reloaData!
            tableView.reloadData()
        } else if notification.name.rawValue == "ContactWasFullyChanged" {
            
            setSortedContactList()
            btnSortContactOnOff()
            btnEditOnOff()
        } else if notification.name.rawValue == "ContactWasDeleted"{
            
            setSortedContactList()
            btnSortContactOnOff()
            btnEditOnOff()
        } else if notification.name.rawValue == "EditedContactWasDeleted" {
            
            setSortedContactList()
            btnEditOnOff()
            btnSortContactOnOff()
            tableView.reloadData()
        }
    }
    
    func setSortedContactList() {
        
        if contactList.returnContactListCount() == 0 {
            
            sortedContacsList = []
        } else if contactList.returnContactListCount() == 1 {
            
            sortedContacsList.append(contactList.getContactByIndex(index: 0))
        } else {
            sortedContacsList = []
            sortedContacsList = contactList.sortContactsBy(typeOfSort: typeOfSort)
            btnSortContacts.title = "SortBySecondName"
            typeOfSort = "SecondName"
            
        }
    }
    
    @IBAction func sortContactsBtn(_ sender: Any) {
        
        if typeOfSort == "FirstName" {
            
            sortedContacsList = contactList.sortContactsBy(typeOfSort: typeOfSort)
            btnSortContacts.title = "SortBySecondName"
            tableView.reloadData()
        } else if typeOfSort == "SecondName" {
            
            sortedContacsList = contactList.sortContactsBy(typeOfSort: typeOfSort)
            btnSortContacts.title = "SortByFirstName"
            tableView.reloadData()
        }
        
        if typeOfSort == "FirstName" {
            
            typeOfSort = "SecondName"
        } else if typeOfSort == "SecondName" {
            
            typeOfSort = "FirstName"
        }
    }
    
    func btnSortContactOnOff() {
        
        let isListEmpty = contactList.returnContactListCount() > 1
        
        self.btnSortContacts.isEnabled = isListEmpty
        self.btnSortContacts.tintColor = isListEmpty ? nil : UIColor.clear
    }
    
    func btnEditOnOff() {
        
        if contactList.returnContactListCount() < 1 {
            
            self.editButtonItem.isEnabled = false
            self.editButtonItem.tintColor = UIColor.clear
            
        } else {
            
            self.editButtonItem.isEnabled = true
            self.editButtonItem.tintColor = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contactList.returnContactListCount()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath)
        let cellContactList = sortedContacsList[indexPath.row]
        let name = cellContactList.getName() + " " + cellContactList.getSurname()
        
        cell.textLabel?.text = name
        cell.detailTextLabel?.text = cellContactList.getPhoneNumber()
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if contactList.returnContactListCount() > indexPath.row {
            
            let neededUiid = sortedContacsList[indexPath.row].getUiid()
            contactList.setShowedContactIndex(uiid: neededUiid)
        }
        return indexPath
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        
        return true
    }
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            debugPrint("sorted contact list index = \(indexPath.row)")
            let neededUiid = sortedContacsList[indexPath.row].getUiid()
            debugPrint("needed Uuid = \(neededUiid)")
            contactList.removeElementByUuid(uuid: neededUiid)
            debugPrint("Index  = \(indexPath.row)")
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
}
