//
//  ContactList.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class ContactList {
    
    private var contactList : [User] = []
    // [OL]: Remove from class interface
    private var contactListInString = String()
    private let saveDownload = SaveDownloadContactList()
    private let contactWasFullyChanged = "ContactWasFullyChanged"
    private let contactWasChanged = "ContactWasChanged"
    private let contactWasDeleted = "ContactWasDeleted"
    private let notifyCenter = NotificationCenter.default
    private var showedContactUuid = String()
    private var contactIsEdited = false
    private var indexOfDeletedContact = Int()
    
    //Function adds contact to the ContactList
    func addContact(contact: User) {
        
        contactList.append(contact)
        notifyCenter.post(name: NSNotification.Name(rawValue: contactWasFullyChanged), object: nil)
    }
    
    //returns sorted array of users by sort type (it would be better to use enum if I want to use other types of sort(Rewrite after tests!))
    func sortContactsBy(typeOfSort: String) -> [User] {
        
        if typeOfSort == "FirstName" {
            return contactList.sorted { ($0.getName() + $0.getSurname()).localizedCaseInsensitiveCompare($1.getName() + $1.getSurname()) == ComparisonResult.orderedAscending }
        } else {
            return contactList.sorted { ($0.getSurname() + $0.getName()).localizedCaseInsensitiveCompare($1.getSurname() + $1.getName()) == ComparisonResult.orderedAscending }
        }
    }
    
    func editContactByUuid (contact: User?){
        
        //Optional not required
        //Use filter method
        if contact != nil {
            var index = 0
            while index < contactList.count {
                if contactList[index].getUiid() == contact?.getUiid() {
                    contactList[index] = contact!
                }
                index += 1
            }
        }
        notifyCenter.post(name: NSNotification.Name(rawValue: contactWasChanged), object: nil)
    }
    
    func getContactByIndex(index: Int) -> User {
        
        return contactList[index]
    }
    
    func getContactByUuid(uuid: String) -> User? {
        
        var returnedContact: User?
        
        indexOfDeletedContact = 0
        for contact in contactList {
            
            indexOfDeletedContact += 1
            if contact.getUiid() == uuid {
                returnedContact = contact
                break
            }
        }
        return returnedContact
    }
    
    func removeElementByUuid(uuid: String) {
        
        let value = getContactByUuid(uuid: uuid)
        debugPrint(value?.getUiid())
        
        for i in contactList {
            debugPrint("Array = \(i.getName())")
            debugPrint(i.getUiid())
        }
        debugPrint(indexOfDeletedContact - 1)
        contactList.remove(at: indexOfDeletedContact - 1)
        saveContactList()
        notifyCenter.post(name: NSNotification.Name(rawValue: contactWasDeleted), object: nil)
    }
    
    func saveContactList(){
        contactListInString = ""
        if contactList.count < 1 {
            
            saveDownload.saveData(stringOfContacts: "")
        } else {
  
            var index = 0
            while index < contactList.count {
                contactListInString.append(contactList[index].getName() + ("*"))
                contactListInString.append(contactList[index].getSurname() + ("*"))
                contactListInString.append(contactList[index].getPhoneNumber() + ("*"))
                contactListInString.append(contactList[index].getEMail() + ("*"))
                contactListInString.append(String(contactList[index].getUiid()) + ("*"))
                
                index += 1
            }
            notifyCenter.post(name: NSNotification.Name(rawValue: contactWasFullyChanged), object: nil)
            saveDownload.saveData(stringOfContacts: contactListInString)
        }
    }
    
    func downloadContacts() {
        
        var downloadedContactList = saveDownload.readData()
        var index = 0
        if downloadedContactList.count > 0 {
            while index < downloadedContactList.count {
                let user = User(name: downloadedContactList[index], surname: downloadedContactList[index + 1],phoneNumber: downloadedContactList[index + 2], eMail: downloadedContactList[index + 3], uuid: downloadedContactList[index + 4])
        
                contactList.append(user)
                index += 5
            }
        } else {
            contactList = [User]()
        }
    }
    
    func getContactIsEdited() -> Bool {
        
        return self.contactIsEdited
    }
    
    func setContactIsEdited(_ state: Bool) {
        
        self.contactIsEdited = state
    }
    
    //returns count of ContactList
    func returnContactListCount() -> Int {
        
        return self.contactList.count
    }
    
    func setShowedContactIndex (uiid: String) {
        
        self.showedContactUuid = uiid
    }
    
    func getContactByShowedContactUuid () -> User {
        
        let user = self.getContactByUuid(uuid: showedContactUuid)
        
        return user!
    }
    
    func getShowedContactUuid() -> String {
        
        return showedContactUuid
    }
}
