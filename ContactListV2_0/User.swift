//
//  User.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class User {
    
    private var name = ""
    private var surname = ""
    private var phoneNumber = ""
    private var eMail = ""
    private var uiid = ""
    
    init(name: String, surname: String, phoneNumber: String, eMail:String, uuid: String) {
        
        self.name = name
        self.surname = surname
        self.phoneNumber = phoneNumber
        self.eMail = eMail
        self.uiid = uuid
    }
    
    func setUiid (uiid: String) {
        
        self.uiid = uiid
    }
    
    func getUiid() -> String{
        
        return self.uiid
    }
    
    func setName (name: String) {
        
        self.name = name
    }
    
    func getName() -> String {
        
        return self.name
    }
    
    func setSurname (surname: String) {
        
        self.surname = surname
    }
    
    func getSurname() -> String {
        
        return self.surname
    }
    
    func setPhoneNumber (phoneNumber: String) {
        
        self.phoneNumber = phoneNumber
    }
    
    func getPhoneNumber() -> String {
        
        return self.phoneNumber
    }
    
    func setEMail (eMail: String) {
        
        self.eMail = eMail
    }
    
    func getEMail() -> String {
        
        return self.eMail
    }
}
