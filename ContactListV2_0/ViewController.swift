//
//  ViewController.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var txtfFirstName: UITextField!
    @IBOutlet weak var txtfSecondName: UITextField!
    @IBOutlet weak var txtfPhoneNumber: UITextField!
    @IBOutlet weak var txtfEmail: UITextField!
    @IBOutlet weak var btnDeleteEditedContact: UIButton!
    private let notifyCenter = NotificationCenter.default
    private let contactWasDeleted = "EditedContactWasDeleted"
    private let editedContactWasEdited = "EditedContactWasEdited"
    
    private let nsNotify = NotificationCenter.default
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    private var contact : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deleteEditedOnOff()
        
        if  contactList.getContactIsEdited() == true {
            contact = contactList.getContactByShowedContactUuid()
            self.title = self.contact!.getName() + " " + self.contact!.getSurname()
            self.txtfFirstName.text = contact?.getName()
            self.txtfSecondName.text = self.contact?.getSurname()
            self.txtfPhoneNumber.text = self.contact?.getPhoneNumber()
            self.txtfEmail.text = self.contact?.getEMail()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnSave(_ sender: Any) {
       
        guard let firstName = txtfFirstName.text else {
            txtfFirstName.becomeFirstResponder()
            return
        }
        guard let lastName = txtfSecondName.text else {
            txtfSecondName.becomeFirstResponder()
            return
        }
        guard let phoneNumber = txtfPhoneNumber.text else {
            txtfPhoneNumber.becomeFirstResponder()
            return
        }
        guard let eMail = txtfEmail.text else {
            txtfEmail.becomeFirstResponder()
            return
        }
        
        
        if contactList.getContactIsEdited() == false {
            
            let createUuid = UUID().uuidString
            contactList.addContact(contact: User(name: firstName, surname: lastName, phoneNumber: phoneNumber, eMail: eMail, uuid: createUuid))
            contactList.saveContactList()
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            
            contact?.setName(name: firstName)
            contact?.setSurname(surname: lastName)
            contact?.setEMail(eMail: eMail)
            contact?.setPhoneNumber(phoneNumber: phoneNumber)
            contactList.editContactByUuid(contact: contact)
            contactList.saveContactList()
            notifyCenter.post(name: NSNotification.Name(rawValue: editedContactWasEdited), object: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        if contactList.getContactIsEdited() == true {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    @IBAction func deleteEditedElement(_ sender: Any) {
        
        contactList.removeElementByUuid(uuid: contactList.getShowedContactUuid())
        notifyCenter.post(name: NSNotification.Name(rawValue: contactWasDeleted), object: nil)
        contactList.setContactIsEdited(state: false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func deleteEditedOnOff() {
        
        if contactList.getContactIsEdited() == true {
            btnDeleteEditedContact.isHidden = false
        } else {
            btnDeleteEditedContact.isHidden = true
        }
    }
}

