//
//  SaveDownloadContactList.swift
//  ContactListV2_0
//
//  Created by Богдан on 18.05.17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

//In my opinion it's better to make it static, but I haven't done it
import Foundation

class SaveDownloadContactList {
    
    let file = "file.txt" //this is the file. we will write to and read from it
    
    func saveData(stringOfContacts: String) {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try stringOfContacts.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
        }
    }
    
    func readData () -> [String] {
        
        var arrayToReturn = [String]()
        var encriptedText = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //reading
            do {
                encriptedText = try String(contentsOf: path, encoding: String.Encoding.utf8)
                
            }
            catch {/* error handling here */}
        }
        
        arrayToReturn = encriptedText.components(separatedBy: "*")
        arrayToReturn.remove(at: arrayToReturn.count - 1)
     
        return arrayToReturn
    }
}

