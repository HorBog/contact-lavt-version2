//
//  ShowInformationViewController.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/18/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class ShowInformationViewController: UIViewController {

    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblSecondName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    private let nc = NotificationCenter.default
    
    private var contact : User?
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contact = contactList.getContactByShowedContactUuid()
        
        setView()
        contactList.setContactIsEdited(true)
        nc.addObserver(forName: NSNotification.Name(rawValue:"EditedContactWasEdited"), object: nil, queue: nil, using: catchNotification)
        // Do any additional setup after loading the view.
    }

    func catchNotification(notification:Notification) -> Void {
        
        lblFirstName.text = self.contact?.getName()
        lblSecondName.text = self.contact?.getSurname()
        lblPhoneNumber.text = self.contact?.getPhoneNumber()
        lblEmail.text = self.contact?.getEMail()
        self.title = self.contact!.getName() + " " + self.contact!.getSurname()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editButton(_ sender: Any) {
        
        contactList.setContactIsEdited(state: true)
    
    }
    
    
    func setView() {
        
        
        lblFirstName.text = self.contact?.getName()
        lblSecondName.text = self.contact?.getSurname()
        lblPhoneNumber.text = self.contact?.getPhoneNumber()
        lblEmail.text = self.contact?.getEMail()
        // [OL]: Move to model
        self.title = self.contact!.getName() + " " + self.contact!.getSurname()
    }
    
    
    @IBAction func returnToRoot(_ sender: Any) {
        contactList.setContactIsEdited(state: false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func editShowedContact(_ sender: Any) {
        
        contactList.setContactIsEdited(state: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
